#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 39
#define LARGE_STATE_COUNT 17
#define SYMBOL_COUNT 19
#define ALIAS_COUNT 0
#define TOKEN_COUNT 11
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 0
#define MAX_ALIAS_SEQUENCE_LENGTH 3
#define PRODUCTION_ID_COUNT 1

enum {
  sym_atom = 1,
  sym_comment = 2,
  anon_sym_LPAREN = 3,
  anon_sym_RPAREN = 4,
  anon_sym_LBRACE = 5,
  anon_sym_RBRACE = 6,
  anon_sym_LBRACK = 7,
  anon_sym_RBRACK = 8,
  anon_sym_DQUOTE = 9,
  aux_sym_string_token1 = 10,
  sym_source_file = 11,
  sym__expression = 12,
  sym_list = 13,
  sym_table = 14,
  sym_seq_table = 15,
  sym_string = 16,
  aux_sym_source_file_repeat1 = 17,
  aux_sym_string_repeat1 = 18,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [sym_atom] = "atom",
  [sym_comment] = "comment",
  [anon_sym_LPAREN] = "(",
  [anon_sym_RPAREN] = ")",
  [anon_sym_LBRACE] = "{",
  [anon_sym_RBRACE] = "}",
  [anon_sym_LBRACK] = "[",
  [anon_sym_RBRACK] = "]",
  [anon_sym_DQUOTE] = "\"",
  [aux_sym_string_token1] = "string_token1",
  [sym_source_file] = "source_file",
  [sym__expression] = "_expression",
  [sym_list] = "list",
  [sym_table] = "table",
  [sym_seq_table] = "seq_table",
  [sym_string] = "string",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym_string_repeat1] = "string_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [sym_atom] = sym_atom,
  [sym_comment] = sym_comment,
  [anon_sym_LPAREN] = anon_sym_LPAREN,
  [anon_sym_RPAREN] = anon_sym_RPAREN,
  [anon_sym_LBRACE] = anon_sym_LBRACE,
  [anon_sym_RBRACE] = anon_sym_RBRACE,
  [anon_sym_LBRACK] = anon_sym_LBRACK,
  [anon_sym_RBRACK] = anon_sym_RBRACK,
  [anon_sym_DQUOTE] = anon_sym_DQUOTE,
  [aux_sym_string_token1] = aux_sym_string_token1,
  [sym_source_file] = sym_source_file,
  [sym__expression] = sym__expression,
  [sym_list] = sym_list,
  [sym_table] = sym_table,
  [sym_seq_table] = sym_seq_table,
  [sym_string] = sym_string,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym_string_repeat1] = aux_sym_string_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym_atom] = {
    .visible = true,
    .named = true,
  },
  [sym_comment] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_LPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RPAREN] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_DQUOTE] = {
    .visible = true,
    .named = false,
  },
  [aux_sym_string_token1] = {
    .visible = false,
    .named = false,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym__expression] = {
    .visible = false,
    .named = true,
  },
  [sym_list] = {
    .visible = true,
    .named = true,
  },
  [sym_table] = {
    .visible = true,
    .named = true,
  },
  [sym_seq_table] = {
    .visible = true,
    .named = true,
  },
  [sym_string] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_string_repeat1] = {
    .visible = false,
    .named = false,
  },
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 9,
  [11] = 5,
  [12] = 7,
  [13] = 4,
  [14] = 6,
  [15] = 3,
  [16] = 2,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 17,
  [26] = 18,
  [27] = 19,
  [28] = 20,
  [29] = 21,
  [30] = 24,
  [31] = 23,
  [32] = 22,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 34,
  [37] = 35,
  [38] = 38,
};

static inline bool sym_comment_character_set_1(int32_t c) {
  return (c < '('
    ? (c < ' '
      ? (c < '\r'
        ? c == '\t'
        : c <= '\r')
      : (c <= ' ' || c == '"'))
    : (c <= ')' || (c < '{'
      ? (c < ']'
        ? c == '['
        : c <= ']')
      : (c <= '{' || c == '}'))));
}

static inline bool sym_atom_character_set_1(int32_t c) {
  return (c < '('
    ? (c < '\r'
      ? (c < '\t'
        ? c == 0
        : c <= '\n')
      : (c <= '\r' || (c < '"'
        ? c == ' '
        : c <= '"')))
    : (c <= ')' || (c < '{'
      ? (c < ']'
        ? c == '['
        : c <= ']')
      : (c <= '{' || c == '}'))));
}

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(2);
      if (lookahead == '"') ADVANCE(11);
      if (lookahead == '(') ADVANCE(5);
      if (lookahead == ')') ADVANCE(6);
      if (lookahead == ';') ADVANCE(3);
      if (lookahead == '[') ADVANCE(9);
      if (lookahead == ']') ADVANCE(10);
      if (lookahead == '{') ADVANCE(7);
      if (lookahead == '}') ADVANCE(8);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(0)
      if (lookahead != 0) ADVANCE(15);
      END_STATE();
    case 1:
      if (lookahead == '\n') SKIP(1)
      if (lookahead == '"') ADVANCE(11);
      if (lookahead == '\\') ADVANCE(14);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(13);
      if (lookahead != 0) ADVANCE(12);
      END_STATE();
    case 2:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 3:
      ACCEPT_TOKEN(sym_comment);
      if (sym_comment_character_set_1(lookahead)) ADVANCE(4);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(3);
      END_STATE();
    case 4:
      ACCEPT_TOKEN(sym_comment);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(4);
      END_STATE();
    case 5:
      ACCEPT_TOKEN(anon_sym_LPAREN);
      END_STATE();
    case 6:
      ACCEPT_TOKEN(anon_sym_RPAREN);
      END_STATE();
    case 7:
      ACCEPT_TOKEN(anon_sym_LBRACE);
      END_STATE();
    case 8:
      ACCEPT_TOKEN(anon_sym_RBRACE);
      END_STATE();
    case 9:
      ACCEPT_TOKEN(anon_sym_LBRACK);
      END_STATE();
    case 10:
      ACCEPT_TOKEN(anon_sym_RBRACK);
      END_STATE();
    case 11:
      ACCEPT_TOKEN(anon_sym_DQUOTE);
      END_STATE();
    case 12:
      ACCEPT_TOKEN(aux_sym_string_token1);
      END_STATE();
    case 13:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '"') ADVANCE(11);
      if (lookahead == '\\') ADVANCE(14);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') ADVANCE(13);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(12);
      END_STATE();
    case 14:
      ACCEPT_TOKEN(aux_sym_string_token1);
      if (lookahead == '"') ADVANCE(12);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(sym_atom);
      if (!sym_atom_character_set_1(lookahead)) ADVANCE(15);
      END_STATE();
    default:
      return false;
  }
}

static bool ts_lex_keywords(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 0},
  [2] = {.lex_state = 0},
  [3] = {.lex_state = 0},
  [4] = {.lex_state = 0},
  [5] = {.lex_state = 0},
  [6] = {.lex_state = 0},
  [7] = {.lex_state = 0},
  [8] = {.lex_state = 0},
  [9] = {.lex_state = 0},
  [10] = {.lex_state = 0},
  [11] = {.lex_state = 0},
  [12] = {.lex_state = 0},
  [13] = {.lex_state = 0},
  [14] = {.lex_state = 0},
  [15] = {.lex_state = 0},
  [16] = {.lex_state = 0},
  [17] = {.lex_state = 0},
  [18] = {.lex_state = 0},
  [19] = {.lex_state = 0},
  [20] = {.lex_state = 0},
  [21] = {.lex_state = 0},
  [22] = {.lex_state = 0},
  [23] = {.lex_state = 0},
  [24] = {.lex_state = 0},
  [25] = {.lex_state = 0},
  [26] = {.lex_state = 0},
  [27] = {.lex_state = 0},
  [28] = {.lex_state = 0},
  [29] = {.lex_state = 0},
  [30] = {.lex_state = 0},
  [31] = {.lex_state = 0},
  [32] = {.lex_state = 0},
  [33] = {.lex_state = 1},
  [34] = {.lex_state = 1},
  [35] = {.lex_state = 1},
  [36] = {.lex_state = 1},
  [37] = {.lex_state = 1},
  [38] = {.lex_state = 0},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [sym_atom] = ACTIONS(1),
    [sym_comment] = ACTIONS(1),
    [anon_sym_LPAREN] = ACTIONS(1),
    [anon_sym_RPAREN] = ACTIONS(1),
    [anon_sym_LBRACE] = ACTIONS(1),
    [anon_sym_RBRACE] = ACTIONS(1),
    [anon_sym_LBRACK] = ACTIONS(1),
    [anon_sym_RBRACK] = ACTIONS(1),
    [anon_sym_DQUOTE] = ACTIONS(1),
  },
  [1] = {
    [sym_source_file] = STATE(38),
    [sym__expression] = STATE(8),
    [sym_list] = STATE(8),
    [sym_table] = STATE(8),
    [sym_seq_table] = STATE(8),
    [sym_string] = STATE(8),
    [aux_sym_source_file_repeat1] = STATE(8),
    [ts_builtin_sym_end] = ACTIONS(3),
    [sym_atom] = ACTIONS(5),
    [sym_comment] = ACTIONS(7),
    [anon_sym_LPAREN] = ACTIONS(9),
    [anon_sym_LBRACE] = ACTIONS(11),
    [anon_sym_LBRACK] = ACTIONS(13),
    [anon_sym_DQUOTE] = ACTIONS(15),
  },
  [2] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(17),
    [sym_comment] = ACTIONS(20),
    [anon_sym_LPAREN] = ACTIONS(23),
    [anon_sym_RPAREN] = ACTIONS(26),
    [anon_sym_LBRACE] = ACTIONS(28),
    [anon_sym_RBRACE] = ACTIONS(26),
    [anon_sym_LBRACK] = ACTIONS(31),
    [anon_sym_RBRACK] = ACTIONS(26),
    [anon_sym_DQUOTE] = ACTIONS(34),
  },
  [3] = {
    [sym__expression] = STATE(10),
    [sym_list] = STATE(10),
    [sym_table] = STATE(10),
    [sym_seq_table] = STATE(10),
    [sym_string] = STATE(10),
    [aux_sym_source_file_repeat1] = STATE(10),
    [sym_atom] = ACTIONS(37),
    [sym_comment] = ACTIONS(39),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_RPAREN] = ACTIONS(43),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [4] = {
    [sym__expression] = STATE(12),
    [sym_list] = STATE(12),
    [sym_table] = STATE(12),
    [sym_seq_table] = STATE(12),
    [sym_string] = STATE(12),
    [aux_sym_source_file_repeat1] = STATE(12),
    [sym_atom] = ACTIONS(51),
    [sym_comment] = ACTIONS(53),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_RBRACE] = ACTIONS(55),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [5] = {
    [sym__expression] = STATE(14),
    [sym_list] = STATE(14),
    [sym_table] = STATE(14),
    [sym_seq_table] = STATE(14),
    [sym_string] = STATE(14),
    [aux_sym_source_file_repeat1] = STATE(14),
    [sym_atom] = ACTIONS(57),
    [sym_comment] = ACTIONS(59),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_RBRACK] = ACTIONS(61),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [6] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_RBRACK] = ACTIONS(67),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [7] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_RBRACE] = ACTIONS(69),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [8] = {
    [sym__expression] = STATE(16),
    [sym_list] = STATE(16),
    [sym_table] = STATE(16),
    [sym_seq_table] = STATE(16),
    [sym_string] = STATE(16),
    [aux_sym_source_file_repeat1] = STATE(16),
    [ts_builtin_sym_end] = ACTIONS(71),
    [sym_atom] = ACTIONS(73),
    [sym_comment] = ACTIONS(75),
    [anon_sym_LPAREN] = ACTIONS(9),
    [anon_sym_LBRACE] = ACTIONS(11),
    [anon_sym_LBRACK] = ACTIONS(13),
    [anon_sym_DQUOTE] = ACTIONS(15),
  },
  [9] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_RPAREN] = ACTIONS(77),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [10] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_RPAREN] = ACTIONS(79),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [11] = {
    [sym__expression] = STATE(6),
    [sym_list] = STATE(6),
    [sym_table] = STATE(6),
    [sym_seq_table] = STATE(6),
    [sym_string] = STATE(6),
    [aux_sym_source_file_repeat1] = STATE(6),
    [sym_atom] = ACTIONS(81),
    [sym_comment] = ACTIONS(83),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_RBRACK] = ACTIONS(85),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [12] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_RBRACE] = ACTIONS(87),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [13] = {
    [sym__expression] = STATE(7),
    [sym_list] = STATE(7),
    [sym_table] = STATE(7),
    [sym_seq_table] = STATE(7),
    [sym_string] = STATE(7),
    [aux_sym_source_file_repeat1] = STATE(7),
    [sym_atom] = ACTIONS(89),
    [sym_comment] = ACTIONS(91),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_RBRACE] = ACTIONS(93),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [14] = {
    [sym__expression] = STATE(2),
    [sym_list] = STATE(2),
    [sym_table] = STATE(2),
    [sym_seq_table] = STATE(2),
    [sym_string] = STATE(2),
    [aux_sym_source_file_repeat1] = STATE(2),
    [sym_atom] = ACTIONS(63),
    [sym_comment] = ACTIONS(65),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_RBRACK] = ACTIONS(95),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [15] = {
    [sym__expression] = STATE(9),
    [sym_list] = STATE(9),
    [sym_table] = STATE(9),
    [sym_seq_table] = STATE(9),
    [sym_string] = STATE(9),
    [aux_sym_source_file_repeat1] = STATE(9),
    [sym_atom] = ACTIONS(97),
    [sym_comment] = ACTIONS(99),
    [anon_sym_LPAREN] = ACTIONS(41),
    [anon_sym_RPAREN] = ACTIONS(101),
    [anon_sym_LBRACE] = ACTIONS(45),
    [anon_sym_LBRACK] = ACTIONS(47),
    [anon_sym_DQUOTE] = ACTIONS(49),
  },
  [16] = {
    [sym__expression] = STATE(16),
    [sym_list] = STATE(16),
    [sym_table] = STATE(16),
    [sym_seq_table] = STATE(16),
    [sym_string] = STATE(16),
    [aux_sym_source_file_repeat1] = STATE(16),
    [ts_builtin_sym_end] = ACTIONS(26),
    [sym_atom] = ACTIONS(103),
    [sym_comment] = ACTIONS(106),
    [anon_sym_LPAREN] = ACTIONS(109),
    [anon_sym_LBRACE] = ACTIONS(112),
    [anon_sym_LBRACK] = ACTIONS(115),
    [anon_sym_DQUOTE] = ACTIONS(118),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 2,
    ACTIONS(121), 1,
      sym_atom,
    ACTIONS(123), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [14] = 2,
    ACTIONS(125), 1,
      sym_atom,
    ACTIONS(127), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [28] = 2,
    ACTIONS(129), 1,
      sym_atom,
    ACTIONS(131), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [42] = 2,
    ACTIONS(133), 1,
      sym_atom,
    ACTIONS(135), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [56] = 2,
    ACTIONS(137), 1,
      sym_atom,
    ACTIONS(139), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [70] = 2,
    ACTIONS(141), 1,
      sym_atom,
    ACTIONS(143), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [84] = 2,
    ACTIONS(145), 1,
      sym_atom,
    ACTIONS(147), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [98] = 2,
    ACTIONS(149), 1,
      sym_atom,
    ACTIONS(151), 8,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_RPAREN,
      anon_sym_LBRACE,
      anon_sym_RBRACE,
      anon_sym_LBRACK,
      anon_sym_RBRACK,
      anon_sym_DQUOTE,
  [112] = 2,
    ACTIONS(121), 1,
      sym_atom,
    ACTIONS(123), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [124] = 2,
    ACTIONS(125), 1,
      sym_atom,
    ACTIONS(127), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [136] = 2,
    ACTIONS(129), 1,
      sym_atom,
    ACTIONS(131), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [148] = 2,
    ACTIONS(133), 1,
      sym_atom,
    ACTIONS(135), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [160] = 2,
    ACTIONS(137), 1,
      sym_atom,
    ACTIONS(139), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [172] = 2,
    ACTIONS(149), 1,
      sym_atom,
    ACTIONS(151), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [184] = 2,
    ACTIONS(145), 1,
      sym_atom,
    ACTIONS(147), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [196] = 2,
    ACTIONS(141), 1,
      sym_atom,
    ACTIONS(143), 6,
      ts_builtin_sym_end,
      sym_comment,
      anon_sym_LPAREN,
      anon_sym_LBRACE,
      anon_sym_LBRACK,
      anon_sym_DQUOTE,
  [208] = 3,
    ACTIONS(153), 1,
      anon_sym_DQUOTE,
    ACTIONS(155), 1,
      aux_sym_string_token1,
    STATE(33), 1,
      aux_sym_string_repeat1,
  [218] = 3,
    ACTIONS(158), 1,
      anon_sym_DQUOTE,
    ACTIONS(160), 1,
      aux_sym_string_token1,
    STATE(35), 1,
      aux_sym_string_repeat1,
  [228] = 3,
    ACTIONS(162), 1,
      anon_sym_DQUOTE,
    ACTIONS(164), 1,
      aux_sym_string_token1,
    STATE(33), 1,
      aux_sym_string_repeat1,
  [238] = 3,
    ACTIONS(166), 1,
      anon_sym_DQUOTE,
    ACTIONS(168), 1,
      aux_sym_string_token1,
    STATE(37), 1,
      aux_sym_string_repeat1,
  [248] = 3,
    ACTIONS(164), 1,
      aux_sym_string_token1,
    ACTIONS(170), 1,
      anon_sym_DQUOTE,
    STATE(33), 1,
      aux_sym_string_repeat1,
  [258] = 1,
    ACTIONS(172), 1,
      ts_builtin_sym_end,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(17)] = 0,
  [SMALL_STATE(18)] = 14,
  [SMALL_STATE(19)] = 28,
  [SMALL_STATE(20)] = 42,
  [SMALL_STATE(21)] = 56,
  [SMALL_STATE(22)] = 70,
  [SMALL_STATE(23)] = 84,
  [SMALL_STATE(24)] = 98,
  [SMALL_STATE(25)] = 112,
  [SMALL_STATE(26)] = 124,
  [SMALL_STATE(27)] = 136,
  [SMALL_STATE(28)] = 148,
  [SMALL_STATE(29)] = 160,
  [SMALL_STATE(30)] = 172,
  [SMALL_STATE(31)] = 184,
  [SMALL_STATE(32)] = 196,
  [SMALL_STATE(33)] = 208,
  [SMALL_STATE(34)] = 218,
  [SMALL_STATE(35)] = 228,
  [SMALL_STATE(36)] = 238,
  [SMALL_STATE(37)] = 248,
  [SMALL_STATE(38)] = 258,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [5] = {.entry = {.count = 1, .reusable = false}}, SHIFT(8),
  [7] = {.entry = {.count = 1, .reusable = true}}, SHIFT(8),
  [9] = {.entry = {.count = 1, .reusable = true}}, SHIFT(3),
  [11] = {.entry = {.count = 1, .reusable = true}}, SHIFT(4),
  [13] = {.entry = {.count = 1, .reusable = true}}, SHIFT(5),
  [15] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [17] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(2),
  [20] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(2),
  [23] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(15),
  [26] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [28] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(13),
  [31] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(11),
  [34] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(36),
  [37] = {.entry = {.count = 1, .reusable = false}}, SHIFT(10),
  [39] = {.entry = {.count = 1, .reusable = true}}, SHIFT(10),
  [41] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(32),
  [45] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [47] = {.entry = {.count = 1, .reusable = true}}, SHIFT(11),
  [49] = {.entry = {.count = 1, .reusable = true}}, SHIFT(36),
  [51] = {.entry = {.count = 1, .reusable = false}}, SHIFT(12),
  [53] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [55] = {.entry = {.count = 1, .reusable = true}}, SHIFT(31),
  [57] = {.entry = {.count = 1, .reusable = false}}, SHIFT(14),
  [59] = {.entry = {.count = 1, .reusable = true}}, SHIFT(14),
  [61] = {.entry = {.count = 1, .reusable = true}}, SHIFT(30),
  [63] = {.entry = {.count = 1, .reusable = false}}, SHIFT(2),
  [65] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [67] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
  [69] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [71] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [73] = {.entry = {.count = 1, .reusable = false}}, SHIFT(16),
  [75] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [77] = {.entry = {.count = 1, .reusable = true}}, SHIFT(17),
  [79] = {.entry = {.count = 1, .reusable = true}}, SHIFT(25),
  [81] = {.entry = {.count = 1, .reusable = false}}, SHIFT(6),
  [83] = {.entry = {.count = 1, .reusable = true}}, SHIFT(6),
  [85] = {.entry = {.count = 1, .reusable = true}}, SHIFT(24),
  [87] = {.entry = {.count = 1, .reusable = true}}, SHIFT(26),
  [89] = {.entry = {.count = 1, .reusable = false}}, SHIFT(7),
  [91] = {.entry = {.count = 1, .reusable = true}}, SHIFT(7),
  [93] = {.entry = {.count = 1, .reusable = true}}, SHIFT(23),
  [95] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [97] = {.entry = {.count = 1, .reusable = false}}, SHIFT(9),
  [99] = {.entry = {.count = 1, .reusable = true}}, SHIFT(9),
  [101] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [103] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(16),
  [106] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(16),
  [109] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(3),
  [112] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(4),
  [115] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(5),
  [118] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(34),
  [121] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_list, 3),
  [123] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_list, 3),
  [125] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_table, 3),
  [127] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_table, 3),
  [129] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string, 3),
  [131] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string, 3),
  [133] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_seq_table, 3),
  [135] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_seq_table, 3),
  [137] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_string, 2),
  [139] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string, 2),
  [141] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_list, 2),
  [143] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_list, 2),
  [145] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_table, 2),
  [147] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_table, 2),
  [149] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_seq_table, 2),
  [151] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_seq_table, 2),
  [153] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_string_repeat1, 2),
  [155] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_string_repeat1, 2), SHIFT_REPEAT(33),
  [158] = {.entry = {.count = 1, .reusable = false}}, SHIFT(29),
  [160] = {.entry = {.count = 1, .reusable = false}}, SHIFT(35),
  [162] = {.entry = {.count = 1, .reusable = false}}, SHIFT(27),
  [164] = {.entry = {.count = 1, .reusable = false}}, SHIFT(33),
  [166] = {.entry = {.count = 1, .reusable = false}}, SHIFT(21),
  [168] = {.entry = {.count = 1, .reusable = false}}, SHIFT(37),
  [170] = {.entry = {.count = 1, .reusable = false}}, SHIFT(19),
  [172] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_sexp_fennel(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .keyword_lex_fn = ts_lex_keywords,
    .keyword_capture_token = sym_atom,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
