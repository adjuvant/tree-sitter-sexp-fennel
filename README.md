# tree-sitter-sexp-fennel

A trivial grammar for s-expressions in context of the [Fennel](https://fennel-lang.org/) programming language.

This is meant to be used as part of [Lissy](https://codeberg.org/adjuvant/vscode-lissy).

For a "better" Fennel grammar, see elsewhere.
For example: [tree-sitter-fennel](https://github.com/TravonteD/tree-sitter-fenne)

## License

The source code of this extension is licensed under the **Mozilla Public License
Version 2.0**. See file LICENSE.
