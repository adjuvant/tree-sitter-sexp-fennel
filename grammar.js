/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 */

module.exports = grammar({
  name: 'sexp_fennel',

  word: $ => $.atom,

  rules: {
    source_file: $ => repeat($._expression),

    _expression: $ => choice(
      $.comment,
      $.list,
      $.table,
      $.seq_table,
      $.string,
      $.atom,
    ),

    comment: $ => /;.*/,

    list: $ => seq(
      '(',
      repeat($._expression),
      ')',
    ),

    table: $ => seq(
      '{',
      repeat($._expression),
      '}',
    ),

    seq_table: $ => seq(
      '[',
      repeat($._expression),
      ']',
    ),

    string: $ => seq('"', repeat(/\\"|./), '"'),

    atom: $ => /[^\s(){}\[\]"]+/,

  }
});
